package com.accenture.dao;

public interface LoginDAO {
	
	public String validateLogin(String userName, String password);

}
