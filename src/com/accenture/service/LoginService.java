package com.accenture.service;

public interface LoginService {

	public String validateLogin(String userName, String password);
}
