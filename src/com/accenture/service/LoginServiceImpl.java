package com.accenture.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accenture.dao.LoginDAO;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private LoginDAO loginRepository;

	public String validateLogin(String userName, String password) {
		return loginRepository.validateLogin(userName, password);
	}

}
